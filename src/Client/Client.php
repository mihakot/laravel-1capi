<?php

namespace Mihakot\Laravel1C\Client;

class Client
{

    private string $path;

    public function __construct()
    {
    }

    /**
     * @param string $path
     * @param array  $data
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $path, array $data = [])
    {
        $host = implode("", [
            "http" . (config('api1c.ssl') ? "s" : "") . "://",
            config('api1c.host') . "/",
            config('api1c.base') . "/",
            "odata/standard.odata/",
        ]);
        $config = [
            // Base URI is used with relative requests
            'base_uri'       => $host,
            'headers'        => [
                'User-Agent' => '1CApiAgent/1.0',
                'Accept'     => 'application/json',
            ],
            'stream'         => true,
            'idn_conversion' => false,
            'auth'           => [config('api1c.user'), config('api1c.password')],
        ];
        $client = new \GuzzleHttp\Client($config);
        $response = $client->request('GET', $path);

        $body = $response->getBody();
        $result = '';
        while (!$body->eof()) {
            $result .= $body->read(1024);
        }

        return json_decode($result)->value;
    }
}