<?php

namespace Mihakot\Laravel1C\Interfaces;

interface API1CInterface
{
    public function get(): \Illuminate\Support\Collection;
    public function post();
}