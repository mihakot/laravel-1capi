<?php

namespace Mihakot\Laravel1C\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class API1C
 *
 * @method static \Mihakot\Laravel1C\Objects\Catalog Catalog(string $entity)
 *
 */
class API1C extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'api1c';
    }
}