# Интеграция Laravel и 1C через API (REST)

[![Latest Version on Packagist](https://img.shields.io/packagist/v/mihakot/laravel-1c-api.svg?style=flat-square)](https://packagist.org/packages/mihakot/laravel-1c-api)
[![Total Downloads](https://img.shields.io/packagist/dt/mihakot/laravel-1c-api.svg?style=flat-square)](https://packagist.org/packages/mihakot/laravel-1c-api)




## Installation

You can install the package via composer:

```bash
composer require mihakot/laravel-1c-api
```

## Usage

```php
// Usage description here
```

### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email mihakot@gmail.com instead of using the issue tracker.

## Credits

-   [Konstantin "MihaKot" Aksarin](https://gitlab.com/mihakot)


## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
